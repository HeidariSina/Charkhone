'use strict';

/**
 *  cmd controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::cmd.cmd');
